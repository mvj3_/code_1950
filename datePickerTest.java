package guan.mac;

import java.util.Calendar;
import java.util.Locale;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.DatePicker;
import android.widget.TimePicker;



public class datePickerTest extends Activity {
	int my_year;
	int my_month;
	int my_day;
	int my_hour;
	int my_minute;
	DatePicker my_dataPicker;
	TimePicker my_timePicker;
	TextView showDate_Time;
	Calendar my_Calendar;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
    	my_Calendar=Calendar.getInstance(Locale.CHINA);
    	my_year=my_Calendar.get(Calendar.YEAR);
    	my_month=my_Calendar.get(Calendar.MONTH);
    	my_day=my_Calendar.get(Calendar.DAY_OF_MONTH);
    	my_hour=my_Calendar.get(Calendar.HOUR_OF_DAY);
    	my_minute=my_Calendar.get(Calendar.MINUTE);
    	
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        my_dataPicker=(DatePicker)findViewById(R.id.datapicker);
        my_timePicker=(TimePicker)findViewById(R.id.timepicker);
        showDate_Time=(TextView)findViewById(R.id.mytextview);
        my_timePicker.setIs24HourView(true);
        loadDate_Time();
        my_dataPicker.init(my_year,my_month,my_day,new DatePicker.OnDateChangedListener(){
        	public void onDateChanged(DatePicker view,int year,int monthOfYear,int dayOfMonth){
        	my_year=year;
        	my_month=monthOfYear;
        	my_day=dayOfMonth;
        	loadDate_Time();
        	}
        });     
        my_timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
			
			public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				my_hour=hourOfDay;
				my_minute=minute;
				loadDate_Time();
			}
		});
    }
        private void loadDate_Time(){
        	showDate_Time.setText(new StringBuffer().append(my_year).append("/").append(FormatString(my_month+1))
        			.append("/").append(FormatString(my_day)).append(" ").append(FormatString(my_hour))
        			.append(".").append(FormatString(my_minute)));
        }
        private String FormatString(int x){
        	String s=Integer.toString(x);
        	if(s.length()==1){
        		s="0"+s;
        	}
        return s;
        }
}
